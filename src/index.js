import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker';
import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <CommentDetail author="Sam" timeAgo="Today at 4:00PM" content="Nice" avatar={faker.image.avatar()}/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="robert" timeAgo="Yesterday" content="Ce bine!" avatar={faker.image.avatar()}/>   
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail author="ana" timeAgo="2005" content="Ura!" avatar={faker.image.avatar()}/>
            </ApprovalCard>
               
            
        </div>

    );
};




ReactDOM.render(<App />, document.querySelector('#root'));


